import './jquery.min.js';
import './bootstrap.min';
import './jquery.sticky.js';
import './click-scroll.js';
import './counter.js';
import './custom.js';
// import './cookieconsent-config';

import.meta.glob([
    '../images/**',
    '../fonts/**'
])
