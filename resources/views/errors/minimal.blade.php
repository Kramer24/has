@extends('layouts.application')

@section('content')
    <body class="antialiased">
    <div class="relative flex items-top justify-center min-h-screen section-bg sm:items-center sm:pt-0">
        <div class="max-w-xl mx-auto sm:px-6 lg:px-8">
            <div class="flex items-center pt-8 sm:justify-start sm:pt-0">
                <div class="ml-4 text-lg text-gray-600 uppercase tracking-wider">
                    @yield('message') | <a href="{{ route('index') }}"> back to home</a>
                </div>
            </div>
        </div>
    </div>
    </body>
@endsection
