<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Children <span class="float-right"><a href="#add-child">+ Add</a></span>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            @forelse(Auth::user()->children as $child)
                <div class="p-4 sm:p-12 bg-white shadow sm:rounded-lg">
                    @include('profile.partials.update-children-information-form', ['children' => $child])
                    <hr>
                </div>
            @empty
                <div class="p-4 sm:p-12 bg-white shadow sm:rounded-lg">
                    <p>No children</p>
                    <hr>
                </div>
            @endforelse
                <div id="add-child" class="p-4 sm:p-12 bg-white shadow sm:rounded-lg">
                    @include('profile.partials.update-children-information-form', ['children' => null])
                    <hr>
                </div>
        </div>
    </div>
</x-app-layout>


<script>
    $(function() {
        $( ".datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>
