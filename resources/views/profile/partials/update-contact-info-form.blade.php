<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900">Contact Info</h2>
        <p class="mt-1 text-sm text-gray-600">Update your contact information.</p>
    </header>

    <form id="send-verification" method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>

    <form method="post" action="{{ route('profile.updateContactInfo') }}" class="mt-6 space-y-6">
        @csrf
        @method('patch')

        <div>
            <x-input-label for="phone" :value="__('Phone')" />
            <x-text-input id="phone" name="phone" type="text" class="mt-1 block w-full" :value="old('phone', $user->contactInfo ? $user->contactInfo->phone : null)" autocomplete="phone" />
            <x-input-error class="mt-2" :messages="$errors->get('phone')" />
        </div>

        <div>
            <x-input-label for="viber" :value="__('Viber')" />
            <x-text-input id="viber" name="viber" type="text" class="mt-1 block w-full" :value="old('viber', $user->contactInfo ? $user->contactInfo->viber : null)" autocomplete="viber" />
            <x-input-error class="mt-2" :messages="$errors->get('viber')" />
        </div>

        <div>
            <x-input-label for="facebook" :value="__('Facebook')" />
            <x-text-input id="facebook" name="facebook" type="text" class="mt-1 block w-full" :value="old('facebook', $user->contactInfo ? $user->contactInfo->facebook : null)" autocomplete="facebook" />
            <x-input-error class="mt-2" :messages="$errors->get('facebook')" />
        </div>

        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>

            @if (session('status') === 'profile-updated')
                <p
                    x-data="{ show: true }"
                    x-show="show"
                    x-transition
                    x-init="setTimeout(() => show = false, 2000)"
                    class="text-sm text-gray-600"
                >Saved.</p>
            @endif
        </div>
    </form>
</section>
