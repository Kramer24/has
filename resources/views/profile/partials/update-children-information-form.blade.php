<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900">@if($children) Child Information @else Add new Child @endif</h2>
    </header>

    <form method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>
    <form method="post" action="{{ $children ? route('children.update', $children) : route('children.store') }}" class="mt-6 space-y-6">
        @csrf
        @if($children)
            @method('patch')
        @endif

        <div class="grid grid-cols-2 gap-3">
            <div>
                <x-input-label for="first_name" :value="__('custom.first_name')" />
                <x-text-input name="first_name" type="text" class="mt-1 block w-full" :value="old('first_name', $children->first_name ?? '')" required autofocus autocomplete="first_name" />
                <x-input-error class="mt-2" :messages="$errors->get('first_name')" />
            </div>

            <div>
                <x-input-label for="last_name" :value="__('custom.last_name')" />
                <x-text-input name="last_name" type="text" class="mt-1 block w-full" :value="old('last_name', $children->last_name ?? '')" required autofocus autocomplete="last_name" />
                <x-input-error class="mt-2" :messages="$errors->get('last_name')" />
            </div>

            <!-- Document Type -->
            <div class="mt-2">
                <x-input-label for="document_type" :value="__('custom.document_type')" />

                <select name="document_type" class="block mt-1 w-full rounded-md shadow-sm border-gray-300" required>
                    <option value="">-- Select --</option>
                    @foreach (App\Enums\DocumentTypeEnum::getDocumentTypes() as $item)
                        <option value="{{ $item }}" @if($children && $children->document_type === $item) selected @endif>
                            {{ $item }}
                        </option>
                    @endforeach
                </select>

                <x-input-error :messages="$errors->get('document_type')" class="mt-2" />
            </div>

            <div class="mt-2">
                <x-input-label for="document" :value="__('Document')" />
                <x-text-input name="document" type="text" class="mt-1 block w-full" :value="old('document', $children->document ?? '')" required autofocus autocomplete="document" />
                <x-input-error class="mt-2" :messages="$errors->get('document')" />
            </div>

            <!-- Gender -->
            <div class="mt-2">
                <x-input-label for="gender" :value="__('Gender')" />

                <select name="gender" class="block mt-1 w-full rounded-md shadow-sm border-gray-300" required>
                    <option value="">-- Select --</option>
                    @foreach (App\Enums\GenderEnum::getGendersArray() as $gender)
                        <option value="{{ $gender }}" @if($children && $children->gender === $gender) selected @endif>
                            {{ $gender }}
                        </option>
                    @endforeach
                </select>

                <x-input-error :messages="$errors->get('gender')" class="mt-2" />
            </div>

            <!-- Birthday -->
            <div class="mt-2">
                <x-input-label for="birthday" :value="__('Birthday')" />
                <x-text-input name="birthday" type="text" class="mt-1 block w-full datepicker" :value="old('birthday', $children->birthday ?? '')" required autofocus autocomplete="birthday" />
                <x-input-error :messages="$errors->get('birthday')" class="mt-2" />
            </div>
        </div>

        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>
            @if($children)
            <x-danger-button
                x-data=""
                x-on:click.prevent="$dispatch('open-modal', 'confirm-children-deletion')"
            >Delete</x-danger-button>
            @endif

            @if (in_array(session('status'), ['children-updated', 'children-created']))
                <p
                    x-data="{ show: true }"
                    x-show="show"
                    x-transition
                    x-init="setTimeout(() => show = false, 2000)"
                    class="text-sm text-gray-600"
                >Saved.</p>
            @endif
            @if (session('status') === 'children-deleted')
                <p
                    x-data="{ show: true }"
                    x-show="show"
                    x-transition
                    x-init="setTimeout(() => show = false, 2000)"
                    class="text-sm text-gray-600"
                >Deleted.</p>
            @endif
        </div>
    </form>

    @if($children)
    <x-modal name="confirm-children-deletion" :show="$errors->childrenDeletion->isNotEmpty()" focusable>
        <form method="post" action="{{ route('children.destroy', $children->id) }}" class="p-6">
            @csrf
            @method('delete')

            <h2 class="text-lg font-medium text-gray-900">Are you sure?</h2>
{{--            <p class="mt-1 text-sm text-gray-600">Once your account is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete your account.</p>--}}

            <div class="mt-6 flex justify-end">
                <x-secondary-button x-on:click="$dispatch('close')">
                    {{ __('Cancel') }}
                </x-secondary-button>

                <x-danger-button class="ml-3">
                    {{ __('Delete') }}
                </x-danger-button>
            </div>
        </form>
    </x-modal>
    @endif
</section>
