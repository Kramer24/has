<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900">Profile Information</h2>
        <p class="mt-1 text-sm text-gray-600">Update your account's profile information and email address.</p>
    </header>

    <form id="send-verification" method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>

    <form method="post" action="{{ route('profile.update') }}" class="mt-6 space-y-6">
        @csrf
        @method('patch')

        <div>
            <x-input-label for="name" :value="__('Name')" />
            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $user->name)" required autofocus autocomplete="name" />
            <x-input-error class="mt-2" :messages="$errors->get('name')" />
        </div>

        <div>
            <x-input-label for="email" :value="__('Email')" />
            <x-text-input id="email" name="email" type="email" class="mt-1 block w-full" :value="old('email', $user->email)" required autocomplete="email" />
            <x-input-error class="mt-2" :messages="$errors->get('email')" />

            @if ($user instanceof \Illuminate\Contracts\Auth\MustVerifyEmail && ! $user->hasVerifiedEmail())
                <div>
                    <p class="text-sm mt-2 text-gray-800">
                        Your email address is unverified.

                        <button form="send-verification" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            {{ __('Click here to re-send the verification email.') }}
                        </button>
                    </p>

                    @if (session('status') === 'verification-link-sent')
                        <p class="mt-2 font-medium text-sm text-green-600">
                            {{ __('A new verification link has been sent to your email address.') }}
                        </p>
                    @endif
                </div>
            @endif
        </div>

        <!-- Document Type -->
        <div class="mt-4">
            <x-input-label for="document_type" :value="__('custom.document_type')" />

            <select name="document_type" id="document-type-dropdown" class="block mt-1 w-full rounded-md shadow-sm border-gray-300" required>
                <option value="">-- Select --</option>
                @foreach (App\Enums\DocumentTypeEnum::getDocumentTypes() as $item)
                    <option value="{{ $item }}" @if($user->document_type === $item) selected @endif>
                        {{ $item }}
                    </option>
                @endforeach
            </select>

            <x-input-error :messages="$errors->get('document_type')" class="mt-2" />
        </div>

        <div>
            <x-input-label for="document" :value="__('Document')" />
            <x-text-input id="document" name="document" type="text" class="mt-1 block w-full" :value="old('document', $user->document)" required autofocus autocomplete="document" />
            <x-input-error class="mt-2" :messages="$errors->get('document')" />
        </div>

        <!-- Gender -->
        <div class="mt-4">
            <x-input-label for="gender" :value="__('Gender')" />

            <select name="gender" id="gender-dropdown" class="block mt-1 w-full rounded-md shadow-sm border-gray-300" required>
                <option value="">-- Select --</option>
                @foreach (App\Enums\GenderEnum::getGendersArray() as $gender)
                    <option value="{{ $gender }}" @if($user->gender === $gender) selected @endif>
                        {{ $gender }}
                    </option>
                @endforeach
            </select>

            <x-input-error :messages="$errors->get('gender')" class="mt-2" />
        </div>

        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>

            @if (session('status') === 'profile-updated')
                <p
                    x-data="{ show: true }"
                    x-show="show"
                    x-transition
                    x-init="setTimeout(() => show = false, 2000)"
                    class="text-sm text-gray-600"
                >Saved.</p>
            @endif
        </div>
    </form>
</section>
