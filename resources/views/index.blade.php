@extends('layouts.application')

@section('title','Home')
@section('og:description', env('APP_DEMO') ? __('demo.our_mission_text') : __('custom.our_mission_text'))

@section('content')
    @include('partial.main-slider')
    @include('partial.welcome-section')
    @include('partial.our-story-block')
    @if(!env('APP_DEMO'))
        @include('partial.ad-block')
    @endif
    @if(env('APP_DEMO'))
        @include('partial.demo.person-block-demo')
    @else
        @include('partial.person-block')
    @endif
{{--    @include('partial.call-to-action')--}}
    @include('partial.upcoming-events-block')
{{--    @include('partial.volunteer-reg-form')--}}
{{--    @include('partial.latest-news-block')--}}
{{--    @include('partial.testimonial-block')--}}
    @include('partial.contact-block')

@endsection
