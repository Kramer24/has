<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'HADAS') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
        {{--    TODO: temp solution--}}
                @if(env('APP_ENV') === 'local')
                    @vite(['resources/css/app.css', 'resources/js/app.js'])
                @else
                    <link rel="stylesheet" type="text/css" href="/build/assets/app.acc3b0d3.css">
                    <link rel="stylesheet" type="text/css" href="/build/assets/bootstrap-icons.css">
                    <script type="text/javascript" src="/build/assets/bootstrap.32611946.js"></script>
                    <script type="text/javascript" src="/build/assets/app.297ec3b5.js"></script>
                @endif
    </head>
    <body>
        <div class="font-sans text-gray-900 antialiased">
            {{ $slot }}
        </div>
    </body>
</html>
