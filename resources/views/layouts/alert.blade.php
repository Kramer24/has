@if (\Session::has('success'))
    <div class="my-2 offset-2 col-8 alert alert-success alert-dismissible fade show" role="alert">
        {!! \Session::get('success') !!}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@elseif(\Session::has('warning'))
    <div class="my-2 offset-2 col-8 text-center alert alert-warning alert-dismissible fade show" role="alert">
        {!! \Session::get('warning') !!}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@elseif(\Session::has('error'))
    <div class="my-2 offset-2 col-8 text-center alert alert-error alert-dismissible fade show" role="alert">
        {!! \Session::get('error') !!}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif

