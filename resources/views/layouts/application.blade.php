<!doctype html>
<html lang="{{ App::getLocale() }}"  theme="{{ setting('site.color_schema') ?: 'original' }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ config('app.name', 'HADAS') }} - @yield('title')</title>
    <meta property="og:image" content="@yield('og:image')" />
    <meta property="og:description" content="@yield('og:description')" />
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    @if(env('APP_ENV') === 'local')
        @vite(['resources/css/application.css', 'resources/js/application.js'])
    @else
        <link rel="stylesheet" type="text/css" href="/build/assets/bootstrap-icons.css">
        <link rel="stylesheet" type="text/css" href="/build/assets/application.c27ae35a.css">
    @endif
{{--    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/orestbida/cookieconsent@v3.0.0-rc.17/dist/cookieconsent.css">--}}

    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
</head>

<body id="section_1">

@include('partial.topbar')
@include('partial.topnavbar')

<main>
@section('content')

@show
</main>
@include('partial.footer')
<script
    src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
    integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
    crossorigin="anonymous"
></script>

@if(env('APP_ENV') === 'public')
    <script src="/build/assets/jquery.min.js"></script>
    <script src="/build/assets/bootstrap.min.js"></script>
    <script src="/build/assets/jquery.sticky.js"></script>
    <script src="/build/assets/click-scroll.js"></script>
    <script src="/build/assets/counter.js"></script>
    <script src="/build/assets/custom.js"></script>
@endif
</body>
</html>
