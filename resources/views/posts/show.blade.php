@extends('layouts.application')

@section('title', 'Post')

@section('content')
    <section class="news-detail-header-section text-center">
        <div class="section-overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-12">
                    <h1 class="text-white">News Detail</h1>
                </div>

            </div>
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-12">
                    <div class="news-block">
                        <div class="news-block-top">
                            <img src="{{ Vite::asset('resources/images/news/medium-shot-volunteers-with-clothing-donations.jpg') }}"
                                 class="news-image img-fluid" alt="">

                            <div class="news-category-block">
                                <a href="#" class="category-block-link">
                                    Lifestyle,
                                </a>

                                <a href="#" class="category-block-link">
                                    Clothing Donation
                                </a>
                            </div>
                        </div>

                        <div class="news-block-info">
                            <div class="d-flex mt-2">
                                <div class="news-block-date">
                                    <p>
                                        <i class="bi-calendar4 custom-icon me-1"></i>
                                        October 12, 2036
                                    </p>
                                </div>
                            </div>

                            <div class="news-block-title mb-2">
                                <h4>Clothing donation to urban area</h4>
                            </div>

                            <div class="news-block-body">
                                <p><strong>Lorem Ipsum</strong> dolor sit amet, consectetur adipsicing kengan omeg kohm
                                    tokito Professional charity theme based on Bootstrap</p>

                                <p><strong>Sed leo</strong> nisl, This is a Bootstrap 5.2.2 CSS template for charity
                                    organization websites. You can feel free to use it. Please tell your friends about
                                    TemplateMo website. Thank you.</p>

                                <blockquote>Sed leo nisl, posuere at molestie ac, suscipit auctor mauris. Etiam quis
                                    metus elementum, tempor risus vel, condimentum orci.
                                </blockquote>
                            </div>

                            <div class="row mt-5 mb-4">
                                <div class="col-lg-6 col-12 mb-4 mb-lg-0">
                                    <img src="{{ Vite::asset('resources/images/news/africa-humanitarian-aid-doctor.jpg') }}"
                                         class="news-detail-image img-fluid" alt="">
                                </div>

                                <div class="col-lg-6 col-12">
                                    <img src="{{ Vite::asset('resources/images/news/close-up-happy-people-working-together.jpg') }}"
                                         class="news-detail-image img-fluid" alt="">
                                </div>
                            </div>

                            <p>You are not allowed to redistribute this template ZIP file on any other template
                                collection website. Please <a href="https://templatemo.com/contact" target="_blank">contact
                                    TemplateMo</a> for more information.</p>

                            <div class="social-share border-top mt-5 py-4 d-flex flex-wrap align-items-center">
                                <div class="tags-block me-auto">

                                </div>

                                <div class="d-flex">
                                    <a href="#" class="social-icon-link bi-facebook"></a>

                                    <a href="#" class="social-icon-link bi-twitter"></a>

                                    <a href="#" class="social-icon-link bi-printer"></a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="fb-share-button"
                         data-href="{{ env('APP_ENV') == 'local' ? 'https://www.hadas.am-saved.info/ru/about-us' : Request::url() }}"
                         data-layout="button_count">
                    </div>
                </div>

                <div class="col-lg-4 col-12 mx-auto mt-4 mt-lg-0">

                    @include('partial.recent-posts')

                </div>

            </div>
        </div>
    </section>

    @include('partial.related-posts')
@endsection
