@extends('layouts.application')

@section('title','News')


@section('content')

    @include('partial.latest-news-block')
    @include('partial.contact-block')

@endsection
