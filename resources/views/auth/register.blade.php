<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-input-label for="name" :value="__('Name')" />

                <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />

                <x-input-error :messages="$errors->get('name')" class="mt-2" />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-input-label for="email" :value="__('Email')" />

                <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />

                <x-input-error :messages="$errors->get('email')" class="mt-2" />
            </div>

            <!-- Document Type -->
            <div class="mt-4">
                <x-input-label for="document_type" :value="__('custom.document_type')" />

                <select name="document_type" id="country-dropdown" class="block mt-1 w-full rounded-md shadow-sm border-gray-300" required>
                    <option value="">-- Select --</option>
                    @foreach (App\Enums\DocumentTypeEnum::getDocumentTypes() as $item)
                        <option value="{{ $item }}" @if(old('document_type') === $item) selected @endif>
                            {{ $item }}
                        </option>
                    @endforeach
                </select>

                <x-input-error :messages="$errors->get('document_type')" class="mt-2" />
            </div>

            <!-- Document -->
            <div class="mt-4">
                <x-input-label for="document" :value="__('Document')" />

                <x-text-input id="document" class="block mt-1 w-full" type="text" name="document" :value="old('document')" required />

                <x-input-error :messages="$errors->get('document')" class="mt-2" />
            </div>

            <!-- Gender -->
            <div class="mt-4">
                <x-input-label for="gender" :value="__('Gender')" />

                <select name="gender" id="country-dropdown" class="block mt-1 w-full rounded-md shadow-sm border-gray-300" required>
                    <option value="">-- Select --</option>
                    @foreach (App\Enums\GenderEnum::getGendersArray() as $gender)
                        <option value="{{ $gender }}" @if(old('gender') === $gender) selected @endif>
                            {{ $gender }}
                        </option>
                    @endforeach
                </select>

                <x-input-error :messages="$errors->get('gender')" class="mt-2" />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-input-label for="password" :value="__('Password')" />

                <x-text-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="new-password" />

                <x-input-error :messages="$errors->get('password')" class="mt-2" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-text-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />

                <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-primary-button class="ml-4">
                    {{ __('Register') }}
                </x-primary-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
