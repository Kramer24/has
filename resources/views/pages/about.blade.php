@extends('layouts.application')

@section('title', __('custom.about'))
@section('og:image', Voyager::image($data->image))
@section('og:description', $data->excerpt)

@section('content')
    <section class="news-detail-header-section text-center">
        <div class="section-overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-12">
                    <h1 class="text-white">{{ __('custom.about') }}</h1>
                </div>

            </div>
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-12">
                    <div class="news-block">
                        <div class="news-block-top">
                            <img src="{{ Voyager::image($data->image) }}" class="news-image img-fluid" alt="">
                        </div>
                        <div class="news-block-info">
                            <div class="news-block-body">
                                {!! $data->getTranslatedAttribute('body', App::getLocale(), 'fallbackLocale') !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-12 mx-auto mt-4 mt-lg-0">
                    Coming soon additional info...
                    <div class="fb-share-button"
                         data-href="{{ env('APP_ENV') == 'local' ? 'https://www.hadas.am-saved.info/ru/about-us' : Request::url() }}"
                         data-layout="button_count">
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
