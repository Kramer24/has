@extends('layouts.application')

@section('title', 'Realise page')

@section('content')
    <section class="news-detail-header-section text-center">
        <div class="section-overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-12">
                    <h1 class="text-white">{{ __('custom.release_page_title') }}</h1>
                </div>

            </div>
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-12">
                    <div class="news-block">
                        <div class="news-block-top">
{{--                            <img src="{{ Voyager::image($data->image) }}" class="news-image img-fluid" alt="">--}}
                        </div>
                        <div class="news-block-info">
                            <div class="news-block-body">
                                <p><strong>0.8.5</strong></p>
                                <ul>
                                    <li>Добавлена страница информации о релизах сервиса (только для демо-версии)</li>
                                    <li>Добавлена ссылка на карту (Google Maps)</li>
                                    <li>Проведена работа над переводами</li>
                                    <li>Работа над Демо-версией</li>
                                    <li>Добавлена возможность выбора цветовой гаммы сайта из настроек</li>
                                    <li>Добавлена цветовая гамма - red</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-12 mx-auto mt-4 mt-lg-0">
                    Coming soon additional info...

                    <div class="fb-share-button"
                         data-href="{{ env('APP_ENV') == 'local' ? 'https://www.hadas.am-saved.info/ru/about-us' : Request::url() }}"
                         data-layout="button_count">
                    </div>
            </div>
        </div>
    </section>

@endsection
