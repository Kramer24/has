<section class="about-section section-padding">
    <div class="container">
        <div class="row">

            <div class="col-lg-3 col-md-6 col-12">
                @if(setting('personblock.image1'))
                    <img src="{{ Voyager::image(setting('personblock.image1')) }}" class="about-image ms-lg-auto bg-light shadow-lg img-fluid" alt="">
                @else
                    <img src="{{ asset('/template/images/portrait-volunteer-who-organized-donations-charity.jpg') }}" class="about-image ms-lg-auto bg-light shadow-lg img-fluid" alt="">
                @endif
            </div>

            <div class="col-lg-3 col-md-6 col-12">
                <div class="custom-text-block">
                    <h2 class="mb-0">{{ \App\Models\SettingTranslation::getTranslation('personblock.title1', App::getLocale()) }}</h2>
                    <p class="text-muted mb-lg-4 mb-md-4">{{ \App\Models\SettingTranslation::getTranslation('personblock.sub_title1', App::getLocale()) }}</p>

                    <ul class="social-icon mt-4">
                        @if(setting('personblock.link_twitter1'))
                            <li class="social-icon-item">
                                <a href="{{ setting('personblock.link_twitter1') }}" class="social-icon-link bi-twitter"></a>
                            </li>
                        @endif
                        @if(setting('personblock.link_facebook1'))
                            <li class="social-icon-item">
                                <a href="{{ setting('personblock.link_facebook1') }}" class="social-icon-link bi-facebook"></a>
                            </li>
                        @endif

                        @if(setting('personblock.link_instagram1'))
                            <li class="social-icon-item">
                                <a href="{{ setting('personblock.link_instagram1') }}" class="social-icon-link bi-instagram"></a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>

{{--            <div class="col-lg-3 col-md-6 col-12">--}}
{{--                @if(setting('personblock.image'))--}}
{{--                    <img src="{{ Voyager::image(setting('personblock.image')) }}" class="about-image ms-lg-auto bg-light shadow-lg img-fluid" alt="">--}}
{{--                @else--}}
{{--                    <img src="{{ asset('/template/images/portrait-volunteer-who-organized-donations-charity.jpg') }}" class="about-image ms-lg-auto bg-light shadow-lg img-fluid" alt="">--}}
{{--                @endif--}}
{{--            </div>--}}

{{--            <div class="col-lg-3 col-md-6 col-12">--}}
{{--                <div class="custom-text-block">--}}
{{--                    <h2 class="mb-0">{{ \App\Models\SettingTranslation::getTranslation('personblock.title2', App::getLocale()) }}</h2>--}}
{{--                    <p class="text-muted mb-lg-4 mb-md-4">{{ \App\Models\SettingTranslation::getTranslation('personblock.sub_title2', App::getLocale()) }}</p>--}}

{{--                    <ul class="social-icon mt-4">--}}
{{--                        @if(setting('personblock.link_twitter'))--}}
{{--                        <li class="social-icon-item">--}}
{{--                            <a href="{{ setting('personblock.link_twitter') }}" class="social-icon-link bi-twitter"></a>--}}
{{--                        </li>--}}
{{--                        @endif--}}
{{--                        @if(setting('personblock.link_facebook'))--}}
{{--                        <li class="social-icon-item">--}}
{{--                            <a href="{{ setting('personblock.link_facebook') }}" class="social-icon-link bi-facebook"></a>--}}
{{--                        </li>--}}
{{--                        @endif--}}

{{--                        @if(setting('personblock.link_instagram'))--}}
{{--                        <li class="social-icon-item">--}}
{{--                            <a href="{{ setting('personblock.link_instagram') }}" class="social-icon-link bi-instagram"></a>--}}
{{--                        </li>--}}
{{--                        @endif--}}
{{--                    </ul>--}}
{{--                </div>--}}
{{--            </div>--}}

        </div>
    </div>
</section>
