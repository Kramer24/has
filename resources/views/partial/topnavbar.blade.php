<nav class="navbar navbar-expand-lg bg-light shadow-lg">
    <div class="container">
        <a class="navbar-brand" href="{{ route('index') }}">
            @if(Voyager::image(setting('site.logo')))
                <img src="{{ Voyager::image(setting('site.logo')) }}" class="logo img-fluid" alt="Kind Heart Charity">
            @else
                <img src="{{ asset('/template/images/logo.png') }}" class="logo img-fluid" alt="Kind Heart Charity">
            @endif
            <span>
                @if(env('APP_DEMO'))
                    {{ __('demo.main_title') }}
                @else
                    {{ __('settings.main_title') }}
                @endif
{{--                <small>{{ __('settings.main_sub_title') }}</small>--}}
            </span>
        </a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('index') }}">{{ __('custom.home') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">{{ __('custom.about') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link smoothscroll" href="{{ route('index') }}#section_2">{{ __('custom.upcoming_events') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('events.index') }}">{{ __('custom.events') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link smoothscroll" href="{{ route('index') }}#section_6">{{ __('custom.contact') }}</a>
                </li>

{{--                <li class="nav-item ms-3">--}}
{{--                    <a class="nav-link custom-btn custom-border-btn btn" href="donate.html">Donate</a>--}}
{{--                </li>--}}
            </ul>
        </div>
    </div>
</nav>
