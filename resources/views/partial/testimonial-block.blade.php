<section class="testimonial-section section-padding section-bg">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-12 mx-auto">
{{--                <h2 class="mb-lg-3">Happy Customers</h2>--}}

                <div id="testimonial-carousel" class="carousel carousel-fade slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        @foreach(setting('quotes') as $quote)
                            <div class="carousel-item active">
                                <div class="carousel-caption">
                                    <h4 class="carousel-title">{{ $quote }}</h4>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
