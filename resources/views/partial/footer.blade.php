<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-12 mb-4">
                @if(Voyager::image(setting('site.logo')))
                    <img src="{{ Voyager::image(setting('site.logo')) }}" class="logo img-fluid" alt="">
                @else
                    <img src="{{ asset('/template/images/logo.png') }}" class="logo img-fluid" alt="">
                @endif
            </div>

            <div class="col-lg-4 col-md-6 col-12 mb-4">
                <h5 class="site-footer-title mb-3">{{ __('custom.links') }}</h5>

                <ul class="footer-menu">
                    <li class="footer-menu-item"><a href="{{ route('about') }}" class="footer-menu-link">{{ __('custom.about') }}</a></li>
                    <li class="footer-menu-item smoothscroll"><a href="{{ route('index') }}#section_2" class="footer-menu-link">{{ __('custom.upcoming_events') }}</a></li>
                    <li class="footer-menu-item"><a href="{{ route('events.index') }}" class="footer-menu-link">{{ __('custom.events') }}</a></li>
                </ul>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mx-auto">
                <h5 class="site-footer-title mb-3">{{ __('custom.contact_information') }}</h5>

                <p class="text-white d-flex mb-2">
                    <i class="bi-telephone me-2"></i>

                    <a href="tel: {{ setting('contacts.phone_1') }}" class="site-footer-link">
                        {{ setting('contacts.phone_1') }}
                    </a>
                </p>

                <p class="text-white d-flex">
                    <i class="bi-envelope me-2"></i>

                    <a href="mailto:info@yourgmail.com" class="site-footer-link">
                        {{ setting('contacts.email_1') }}
                    </a>
                </p>

                <p class="text-white d-flex mt-3">
                    <i class="bi-geo-alt me-2"></i>
                    {{ setting('contacts.address_1') }}
                </p>

                @if(setting('contacts.map_code'))
                    <a href="{{ setting('contacts.map_code') }}" target="_blank" class="custom-btn btn mt-3">{{ __('custom.view_map') }}</a>
                @endif
            </div>
        </div>
    </div>

    <div class="site-footer-bottom">
        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-md-7 col-12">
                    <p class="copyright-text mb-0">Copyright © 2022-{{ date('Y') }}
                        Humanitarian aid distribution accounting system - GheFa Development
                        <a target="_blank" href="{{ route('voyager.dashboard') }}">
                            <span><i class="bi-lock"></i></span>
                        </a>
                    </p>
                </div>

                <div class="col-lg-5 col-md-5 col-12 d-flex justify-content-center align-items-center mx-auto">
                    <ul class="social-icon">
                        @if(setting('personBlock.link_twitter'))
                        <li class="social-icon-item">
                            <a href="#" class="social-icon-link bi-twitter"></a>
                        </li>
                        @endif
                        @if(setting('personBlock.link_facebook'))
                        <li class="social-icon-item">
                            <a href="#" class="social-icon-link bi-facebook"></a>
                        </li>
                        @endif
                        @if(setting('personBlock.link_instagram'))
                        <li class="social-icon-item">
                            <a href="#" class="social-icon-link bi-instagram"></a>
                        </li>
                        @endif
                    </ul>
                    @if(setting('site.version'))
                        <small class="text-white copyright-text mr-2">
                            {{ __('custom.version') }}: {{ setting('site.version') }}
                        </small>
                    @endif
                    @if(env('APP_DEMO'))
                        <a href="{{ route('release') }}" class="copyright-text">All releases</a>
                    @endif
                </div>

            </div>
        </div>
    </div>
</footer>
