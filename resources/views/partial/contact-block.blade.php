<section class="contact-section section-padding" id="section_6">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-12 ms-auto mb-5 mb-lg-0">
                <div class="contact-info-wrap">
                    <h2>{{ __('custom.get_in_touch') }}</h2>

                    <div class="contact-image-wrap d-flex flex-wrap">
{{--                        @if(setting('personblock.image'))--}}
{{--                            <img src="{{ Voyager::image(setting('personblock.image')) }}" class="img-fluid avatar-image" alt="">--}}
{{--                        @else--}}
{{--                            <img src="{{ asset('/template/images/portrait-volunteer-who-organized-donations-charity.jpg') }}" class="img-fluid avatar-image" alt="">--}}
{{--                        @endif--}}

                        <div class="d-flex flex-column justify-content-center ms-3">
{{--                            <p class="mb-0">{{ \App\Models\SettingTranslation::getTranslation('personblock.title2', App::getLocale()) }}</p>--}}
{{--                            <p class="mb-0"><strong>{{ \App\Models\SettingTranslation::getTranslation('personblock.sub_title2', App::getLocale()) }}</strong></p>--}}
                            <p class="mb-0">{{ trans('settings.personblock.worktime') }}</p>
                            <p class="mb-0"><i class="bi-telephone me-2"></i> {{ setting('contacts.phone_2') }}</p>
{{--                            <p class="mb-0"><i class="bi-envelope me-2"></i> {{ setting('contacts.email_2') }}</p>--}}
                        </div>
                    </div>

                    <div class="contact-info">
                        <h5 class="mb-3">{{ __('custom.contact_information') }}</h5>

                        <p class="d-flex mb-2">
                            <i class="bi-geo-alt me-2"></i>
                            {{ setting('contacts.address_1') }}
                        </p>

                        <p class="d-flex mb-2">
                            <i class="bi-telephone me-2"></i>

                            <a href="tel: 120-240-9600">
                                {{ setting('contacts.phone_1') }}
                            </a>
                        </p>

                        <p class="d-flex">
                            <i class="bi-envelope me-2"></i>

                            <a href="mailto:{{ setting('contacts.email_1') }}">
                                {{ setting('contacts.email_1') }}
                            </a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 col-12 mx-auto">
                <form class="custom-form contact-form" action="{{ route('addMessage') }}"
                enctype="multipart/form-data" method="post" role="form">
                @csrf

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <input type="text" name="name" id="first-name" class="form-control" placeholder="{{ __('custom.name') }}" required>
                        </div>

                        <div class="col-lg-6 col-md-6 col-12">
                            <input type="text" name="surname" id="last-name" class="form-control" placeholder="{{ __('custom.surname') }}" required>
                        </div>
                    </div>
                    <input type="email" name="email" id="email" pattern="[^ @]*@[^ @]*" class="form-control" placeholder="{{ __('custom.email') }}" required>
                    <textarea name="message" rows="5" class="form-control" id="message" placeholder="{{ __('custom.who_help') }}"></textarea>

                    <button type="submit" class="form-control">{{ __('custom.send_message') }}</button>
                </form>
            </div>

        </div>
    </div>
</section>
