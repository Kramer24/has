<section class="section-padding section-bg" id="section_2">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-12 text-center mb-4">
                <h2>{{ __('custom.upcoming_events') }}</h2>
            </div>

            @if(count($events) > 0)
                @foreach($events as $event)

                <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0">
                    <div class="custom-block-wrap">
                        @if($event->image)
                            <img src="{{ Voyager::image($event->image) }}" class="custom-block-image img-fluid" alt="">
                        @else
                            <img src="{{ asset('/template/images/news/medium-shot-volunteers-with-clothing-donations.jpg') }}" class="custom-block-image img-fluid" alt="">
                        @endif
                        <div class="custom-block">
                            <div class="custom-block-body">
                                <h5 class="mb-3">{{ $event->getTranslatedAttribute('title', App::getLocale(), 'fallbackLocale') }}</h5>
                                <p>{{ $event->getTranslatedAttribute('excerpt', App::getLocale(), 'fallbackLocale') }}</p>
                                <div class="progress mt-4">
                                    <div class="progress-bar w-100" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <div class="my-2">
                                    <p class="mb-0">
                                        <strong>{{ __('custom.start') }}:</strong>
                                        {{ date('Y-m-d H:i', strtotime($event->event_start)) }}
                                    </p>
                                    @if($event->event_end)
                                    <p class="mb-0">
                                        <strong>{{ __('custom.end') }}:&nbsp;&nbsp;</strong>
                                        {{ date('Y-m-d H:i', strtotime($event->event_end)) }}
                                    </p>
                                    @endif
                                </div>
                                @if($event->age_from || $event->age_to)
                                <div class="my-2">
                                    <p class="mb-0">
                                        <strong>{{ __('custom.age_restrictions') }}:</strong>
                                        {{ $event->age_from . ' - ' }}{{ $event->age_to }}
                                    </p>
                                </div>
                                @endif
                            </div>
                            <a href="{{ route('events.show',$event) }}" class="custom-btn btn">{{ __('custom.see_more') }}</a>
                        </div>
                    </div>
                </div>

                @endforeach
            @else
                {{ __('custom.no_events') }}
            @endif

        </div>
    </div>
</section>
