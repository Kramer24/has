<section class="section-padding">
    <div class="container">
        <div class="row">

            <div class="col-lg-10 col-12 text-center mx-auto">
                @if(env('APP_DEMO'))
                    <h2 class="mb-5">{{ __('demo.welcome_title') }}</h2>
                @else
                    <h2 class="mb-5">{{ __('custom.welcome_title') }}</h2>
                @endif
            </div>

            <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0">
                <div class="featured-block d-flex justify-content-center align-items-center">
                    <a class="d-block">
                        <img src="{{ Vite::asset('resources/images/icons/hands.png') }}" class="featured-block-image img-fluid" alt="">
                        <p class="featured-block-text">{!! __('custom.become_volunteer') !!}</p>
                    </a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0 mb-md-4">
                <div class="featured-block d-flex justify-content-center align-items-center">
                    <a class="d-block">
                        <img src="{{ Vite::asset('resources/images/icons/heart.png') }}" class="featured-block-image img-fluid" alt="">

                        <p class="featured-block-text">{!! __('custom.caring_love') !!}</p>
                    </a>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0 mb-md-4">
                <div class="featured-block d-flex justify-content-center align-items-center">
                    <a class="d-block">
                        <img src="{{ Vite::asset('resources/images/icons/receive.png') }}" class="featured-block-image img-fluid" alt="">

                        <p class="featured-block-text">{!! __('custom.make_donation') !!}</p>
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>
