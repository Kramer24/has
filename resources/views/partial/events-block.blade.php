<section class="news-section section-padding">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-12 mb-5">
                <h2>{{ __('custom.events') }}</h2>
            </div>

            <div class="col-lg-7 col-12">
                @if(count($events) > 0)
                    @foreach($events as $event)
                        <div class="news-block mb-1">
                            <div class="news-block-top">
                                <a href="{{ route('events.show', $event) }}">
                                    @if($event->image)
                                        <img src="{{ Voyager::image($event->image) }}" class="news-image img-fluid" alt="">
                                    @else
                                        <img src="{{ asset('/template/images/news/medium-shot-volunteers-with-clothing-donations.jpg') }}" class="news-image img-fluid" alt="">
                                    @endif
                                </a>
                            </div>

                            <div class="news-block-info">
                                <div class="d-flex mt-2">
                                    <div class="news-block-date">
                                        <p>
                                            <i class="bi-calendar4 custom-icon me-1"></i>
                                            {{ $event->event_start }}
                                        /
                                            <i class="bi-calendar4 custom-icon me-1"></i>
                                            {{ $event->event_end }}
                                        </p>
                                    </div>
                                </div>

                                <div class="news-block-title mb-2">
                                    <h4>
                                        <a href="{{ route('events.show', $event) }}" class="news-block-title-link">
                                            {{ $event->getTranslatedAttribute('title', App::getLocale(), 'fallbackLocale') }}
                                        </a>
                                    </h4>
                                </div>

                                <div class="news-block-body">
                                    <p>{{ $event->getTranslatedAttribute('excerpt', App::getLocale(), 'fallbackLocale')}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                        <div class="text-center">{{ $events->links() }}</div>
                @else
                    {{ __('custom.not_found_active_events') }}
                @endif
            </div>

            <div class="col-lg-4 col-12 mx-auto">

               @include('partial.recent-posts')

{{--                <div class="category-block d-flex flex-column">--}}
{{--                    <h5 class="mb-3">{{ trans_choice('custom.category',2) }}</h5>--}}

{{--                    <a href="#" class="category-block-link">--}}
{{--                        Drinking water--}}
{{--                        <span class="badge">20</span>--}}
{{--                    </a>--}}

{{--                    <a href="#" class="category-block-link">--}}
{{--                        Food Donation--}}
{{--                        <span class="badge">30</span>--}}
{{--                    </a>--}}

{{--                    <a href="#" class="category-block-link">--}}
{{--                        Children Education--}}
{{--                        <span class="badge">10</span>--}}
{{--                    </a>--}}

{{--                    <a href="#" class="category-block-link">--}}
{{--                        Poverty Development--}}
{{--                        <span class="badge">15</span>--}}
{{--                    </a>--}}

{{--                    <a href="#" class="category-block-link">--}}
{{--                        Clothing Donation--}}
{{--                        <span class="badge">20</span>--}}
{{--                    </a>--}}
{{--                </div>--}}

            </div>

        </div>
    </div>
</section>
