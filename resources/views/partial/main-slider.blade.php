<section class="hero-section hero-section-full-height">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12 col-12 p-0">
                <div id="hero-slide" class="carousel carousel-fade slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        @if($sliders->isEmpty())
                        <div class="carousel-item active">
                            <img src="{{ Vite::asset('resources/images/slide/volunteer-helping-with-donation-box.jpg') }}" class="carousel-image img-fluid" alt="...">

                            <div class="carousel-caption d-flex flex-column justify-content-end">
                                <h1>Default slider</h1>
                                <p>Professional approach to charity</p>
                            </div>
                        </div>
                        @else
                            @foreach($sliders as $slider)
                                <div class="carousel-item active">
                                    <img src="{{ Voyager::image($slider->image) }}" class="carousel-image img-fluid" alt="...">
                                    <div class="carousel-caption d-flex flex-column justify-content-end">
                                        <h1>{{ $slider->getTranslatedAttribute('title', App::getLocale(), 'fallbackLocale') }}</h1>
                                        <p>{{ $slider->getTranslatedAttribute('subtitle', App::getLocale(), 'fallbackLocale') }}</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                    <button class="carousel-control-prev" type="button" data-bs-target="#hero-slide" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>

                    <button class="carousel-control-next" type="button" data-bs-target="#hero-slide" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>

        </div>
    </div>
</section>
