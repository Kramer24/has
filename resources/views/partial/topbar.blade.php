<header class="site-header">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-12 d-flex flex-wrap">
                <p class="d-flex me-4 mb-0">
                    <i class="bi-geo-alt me-2"></i>
                    {{ setting('contacts.address_1') }}
                </p>

                <p class="d-flex me-4 mb-0">
                    <i class="bi-envelope me-2"></i>
                    <a href="mailto:{{ setting('contacts.email_1') }}">
                        {{ setting('contacts.email_1') }}
                    </a>
                </p>

                <p class="d-flex me-4 mb-0">
                    <i class="bi-telephone me-2"></i>
                    <a href="tel: {{ setting('contacts.phone_1') }}">
                        {{ setting('contacts.phone_1') }}
                    </a>
                </p>

                <p class="d-flex mb-0">
                    @if(Auth::check())
                        <a href="{{ route('dashboard') }}" target="_blank">{{ __('custom.account') }}</a>
                    @else
                        <a href="{{ route('login') }}" target="_blank">{{ __('custom.login') }}</a>
                    @endif
                </p>
            </div>
            <div class="col-lg-2 col-12 ms-auto d-lg-block d-none">
                <ul class="social-icon">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        <li class="social-icon-item">
                            <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" class="social-icon-link">
                                {{ $localeCode }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="col-lg-2 col-12 ms-auto d-lg-block d-none">
                <ul class="social-icon">
                    @if(setting('site.facebook'))
                        <li class="social-icon-item">
                            <a href="{{ setting('site.facebook') }}" target="_blank" class="social-icon-link bi-facebook"></a>
                        </li>
                    @endif
                    @if(setting('site.instagram'))
                        <li class="social-icon-item">
                            <a href="{{ setting('site.instagram') }}" target="_blank" class="social-icon-link bi-instagram"></a>
                        </li>
                    @endif
{{--                    <li class="social-icon-item">--}}
{{--                        <a href="#" class="social-icon-link bi-youtube"></a>--}}
{{--                    </li>--}}

{{--                    <li class="social-icon-item">--}}
{{--                        <a href="#" class="social-icon-link bi-whatsapp"></a>--}}
{{--                    </li>--}}
                </ul>
            </div>

        </div>
    </div>
</header>
