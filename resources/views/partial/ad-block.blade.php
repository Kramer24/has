<section class="volunteer-section section-padding">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-md-8 col-12">
                <p>
                    <video class="video-block" controls="controls">
                        <source src="https://armiyatanaspasenieto.org/video/family_meet.mp4" type="video/mp4">
                    </video>
                </p>
            </div>

            <div class="col-lg-4 col-md-4 col-12">
                <div class="custom-text-block">
                    <h2 class="text-white mb-0">{{ \App\Models\SettingTranslation::getTranslation('ad-block.title', App::getLocale()) }}</h2>
                    <p class="text-white mb-lg-4 mb-md-4">{{ \App\Models\SettingTranslation::getTranslation('ad-block.subtitle', App::getLocale()) }}</p>
                </div>
            </div>
        </div>
    </div>
</section>
