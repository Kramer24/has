<h5 class="mb-3">{{ __('custom.recent_events') }}</h5>
@foreach ($recentEvents as $event)
    <div class="news-block news-block-two-col d-flex mt-4">
        <div class="news-block-two-col-image-wrap">
            <a href="{{ route('events.show', $event) }}">
                @if($event->image)
                    <img src="{{ Voyager::image($event->image) }}" class="news-image img-fluid" alt="">
                @else
                    <img src="{{ asset('/template/images/news/medium-shot-volunteers-with-clothing-donations.jpg') }}" class="news-image img-fluid" alt="">
                @endif
            </a>
        </div>

        <div class="news-block-two-col-info">
            <div class="news-block-title mb-2">
                <h6>
                    <a href="{{ route('events.show', $event ) }}" class="news-block-title-link">
                        {{ $event->getTranslatedAttribute('title', App::getLocale(), 'fallbackLocale') }}
                    </a>
                </h6>
            </div>

            <div class="news-block-date">
                <p>
                    <i class="bi-calendar4 custom-icon me-1"></i>
                    {{ $event->event_start }}
                </p>
            </div>
        </div>
    </div>
@endforeach



