<x-mail::message>
    <h1>Hello you have a new message from:</h1>
    <h2>Name: {{ $message->name }}</h2>
    <h2>Surname: {{ $message->surname }}</h2>
    <h2>Email: {{ $message->email }}</h2>
    <h2>Message:</h2>
    <p>{{ $message->message }}</p>
    {{ config('app.name') }}
</x-mail::message>
