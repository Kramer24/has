<x-mail::message>
    Dear, {{ $user->name }}<br>
    @if($message)
        {{ $message }}
    @else
        You are invited to the event
    @endif
<x-mail::button :url="route('events.show', $event)">
    See more
</x-mail::button>
    Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
