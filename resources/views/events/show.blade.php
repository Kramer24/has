@extends('layouts.application')

@section('title', $event->getTranslatedAttribute('title', App::getLocale(), 'fallbackLocale'))
@section('og:image', Voyager::image($event->image))
@section('og:description', $event->excerpt)

@section('content')
    <section class="news-detail-header-section text-center">
        <div class="section-overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-12">
                    <h1 class="text-white">{{ __('custom.event_details') }}</h1>
                </div>

            </div>
        </div>
    </section>

    <section class="news-section section-padding">
        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-12">
                    <div class="news-block">
                        <div class="news-block-top">
                            @if($event->image)
                                <img src="{{ Voyager::image($event->image) }}" class="news-image img-fluid" alt="">
                            @else
                                <img src="{{ asset('/template/images/news/medium-shot-volunteers-with-clothing-donations.jpg') }}" class="news-image img-fluid" alt="">
                            @endif
                        </div>

                        <div class="news-block-info">
                            <div class="d-flex mt-2">
                                <div class="news-block-date">
                                    <p>
                                        <i class="bi-calendar4 custom-icon me-1"></i>
                                        {{ $event->event_start }}
                                        @if($event->event_end)
                                        /
                                        <i class="bi-calendar4 custom-icon me-1"></i>
                                        {{ $event->event_end }}
                                        @endif
                                    </p>
                                </div>
                            </div>
                            @if($event->age_from || $event->age_to)
                                <div class="d-flex mt-2">
                                    <div class="news-block-date">
                                        <p>
                                            <strong>{{ __('custom.age_restrictions') }}:</strong>
                                            {{ $event->age_from . ' - ' }}{{ $event->age_to }}
                                        </p>
                                    </div>
                                </div>
                            @endif

                            <div class="news-block-title mb-2">
                                <h4>{{ $event->getTranslatedAttribute('title', App::getLocale(), 'fallbackLocale') }}</h4>
                            </div>

                            <div class="news-block-body">
                                {!! $event->getTranslatedAttribute('description', App::getLocale(), 'fallbackLocale') !!}
                            </div>

                            {{--                            <div class="social-share border-top mt-5 py-4 d-flex flex-wrap align-items-center">--}}

                            {{--                            </div>--}}

                        </div>
                    </div>
                    @if($event->status === 'REGISTRATION' and $event->use_registration)
                        @php($showRegisterButton = false)
                        @include('layouts.alert')
                        @if(!Auth::check())
                           <button id="send-part-form" type="submit" class="form-control mt-4">
                                <a href="{{ route('register') }}" target="_blank">{{ __('custom.login') }}</a>
                            </button>
                        @else
                            <hr>
                            <form class="custom-form col-lg-8 col-8 offset-2" action="{{ route('eventParticipate', $event) }}"
                          method="post" role="form">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $event->id }}" name="event_id">
                        <input type="hidden" value="{{ Auth::id() }}" name="user_id">

                        @if((!$event->age_from and !$event->age_to) or $event->age_from >= 18 )
                        <label>
                            <input type="checkbox" @if($partCurrentEvent) disabled @endif name="selected-me"
                                   class="select-participant"
                                   @if(!empty($partCurrentEvent->users['user']) && $partCurrentEvent->users['user'] == Auth::id()) checked @endif
                                   value="{{ \Illuminate\Support\Facades\Auth::id() }}">
                            {{ __('custom.me') }}
                        </label>
                        <br>
                            @php($showRegisterButton = true)
                        @endif
                        @foreach(Auth::user()->children as $child)
                            @if((!$event->age_from || $child->age >= $event->age_from)
                                    && (!$event->age_to || $child->age <= $event->age_to))
                                        <label>
                                            <input type="checkbox" @if($partCurrentEvent) disabled @endif
                                            name="selected-children[]" class="select-participant" value="{{ $child->id }}"
                                            @if(isset($partCurrentEvent->users['children']) && in_array($child->id, $partCurrentEvent->users['children'])) checked @endif>
                                            {{ $child->full_name }}
                                        </label>
                                        <br>
                                        @php($showRegisterButton = true)
                            @endif
                        @endforeach
                        @if($showRegisterButton)
                            <button id="send-part-form" type="submit" @if($partCurrentEvent) disabled @endif class="form-control mt-4">
                                {{ __('custom.register') }}
                            </button>
                        @endif

                        </form>
                        @endif
                        @if($partCurrentEvent)
                            <p class="text-center">
                                {{ __('custom.you_already_registered') }} <br> {{ __('custom.your_status') }} - {{ $partCurrentEvent->status }} <br>
                                {!! __('custom.change_choice') !!}
                            </p>
                        @endif
                    @elseif($event->status === 'FINISHED')
                        <h5 class="text-center">
                            {{ __('custom.event_finished') }}
                        </h5>
                    @elseif($event->status === 'PUBLISHED' and $event->use_registration)
                        @if($partCurrentEvent)
                            <p class="text-center">
                                {{ __('custom.you_already_registered') }} <br> {{ __('custom.your_status') }} - {{ $partCurrentEvent->status }} <br>
                            </p>
                        @endif
                    @endif
                    <hr>
                    <div class="fb-share-button"
                         data-href="{{ env('APP_ENV') == 'local' ? 'https://www.hadas.am-saved.info/ru/about-us' : Request::url() }}"
                         data-layout="button_count">
                    </div>
                </div>

                <div class="col-lg-4 col-12 mx-auto mt-4 mt-lg-0">
                    @include('partial.recent-posts')
                </div>

            </div>
        </div>
    </section>
@endsection
<script>
        document.addEventListener('click', function (event) {
            if (!event.target.matches('#change-choice')) return;

            event.preventDefault();
            const boxes = document.querySelectorAll('.select-participant');
            boxes.forEach((element) => {
                element.removeAttribute('disabled')
            })
            document.getElementById('send-part-form').removeAttribute('disabled')
        }, false);
</script>
