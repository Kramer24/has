@extends('layouts.application')

@section('title','Events')
@section('og:image', '/template/images/logo.png')
@section('og:description', env('APP_DEMO') ? __('demo.our_mission_text') : __('custom.our_mission_text'))

@section('content')

    @include('partial.events-block')
    @include('partial.contact-block')

@endsection
