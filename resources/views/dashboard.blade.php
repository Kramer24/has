<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="mt-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    You're logged in!
                </div>
            </div>
        </div>
    </div>
    <div class="max-w-7xl py-6 flex mx-auto">
        <div class=" sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <section>
                        <header>
                            <h2 class="text-lg font-medium text-gray-900 mb-2">Participation in events</h2>
                            <hr>
                            @foreach($events as $event)
                                <p class="mt-1">
                                    {{ $event->title }}
                                </p>
                                <p class="mt-1 text-sm text-gray-600">
                                    <b>Event start:</b> {{ $event->event_start }}<br>
                                    <b>Event end:</b> {{ $event->event_end }}
                                </p>
                                <p class="mt-1 mb-1 text-sm text-gray-600">
                                    <b>Status:</b> {{ $event->pivot->status }}
                                </p>
                                <hr>
                            @endforeach
                        </header>
                    </section>
                </div>
            </div>
        </div>
        <div class="sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <section>
                        <header>
                            <h2 class="text-lg font-medium text-gray-900">Children</h2>
                            @foreach($children as $child)
                                <p class="mt-1">
                                    <b>Full name: </b>{{ $child->full_name }}<br>
                                    <b>Birthday: </b>{{ $child->birthday }}

                                </p>
                                <hr>
                            @endforeach
                        </header>
                    </section>
                </div>
            </div>
        </div>
        <div class="sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <section>
                        <header>
                            <h2 class="text-lg font-medium text-gray-900">Additional Information</h2>
                            <p class="mt-1 text-sm text-gray-600">Later...</p>
                        </header>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
