<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\Voyager\ParticipantController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\ChildrenController;
//use App\Http\Controllers\Voyager\VoyagerSettingsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function() {
        Route::get('/', [PageController::class,'index'])->name('index');
        Route::get('/about-us', [PageController::class,'about'])->name('about');
        Route::get('/release', [PageController::class,'release'])->name('release');
        Route::resource('/posts', PostController::class)
            ->only(['index', 'show'])
            ->names('posts');
        Route::resource('/events', EventController::class)
            ->only(['index', 'show'])
            ->names('events');
        Route::post('/add-message', [MessageController::class, 'addMessage'])
            ->name('addMessage');
});

Route::post('/event-participate/{event}', [EventController::class, 'onParticipate'])->middleware('auth')->name('eventParticipate');

// Panel pages
Route::group(['prefix' => 'panel'], function () {
    Voyager::routes();
    Route::get('/events/{event}/participants',[ParticipantController::class,'indexByEvent'])->name('voyager.participants.indexByEvent');
    Route::get('/to-accept-participant/{participant}',[ParticipantController::class,'toAccept'])->name('voyager.participants.toAccept');
    Route::get('/to-reject-participant/{participant}',[ParticipantController::class,'toReject'])->name('voyager.participants.toReject');
    Route::get('/to-draft-participant/{participant}',[ParticipantController::class,'toDraft'])->name('voyager.participants.toDraft');
    Route::get('/to-invite-participant/{participant}',[ParticipantController::class,'toInvite'])->name('voyager.participants.toInvite');
    Route::post('/to-invite-participant-bulk/{event}',[ParticipantController::class,'toInviteBulk'])->name('voyager.participants.toInviteBulk');
    Route::get('/to-service-participant/{participant}',[ParticipantController::class,'toService'])->name('voyager.participants.toService');

    Route::get('/events/{event}/participants/export/', [ExportController::class, 'exportParticipantsEvent'])->name('export.participants.event');
});

// Profile pages
Route::get('/dashboard', [ProfileController::class, 'show'])->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::patch('/update-contact-info', [ProfileController::class, 'updateContactInfo'])->name('profile.updateContactInfo');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::resource('/children', ChildrenController::class)->only(['index','store']);
    Route::patch('/children/{children}', [ChildrenController::class, 'update'])->name('children.update');
    Route::delete('/children/{children}', [ChildrenController::class, 'destroy'])->name('children.destroy');

});

require __DIR__.'/auth.php';
