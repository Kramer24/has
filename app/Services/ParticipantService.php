<?php

namespace App\Services;

use App\Enums\ParticipantStatusEnum;
use App\Mail\InvitePartMail;
use App\Models\Event;
use App\Models\Participant;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ParticipantService
{
    public function changeStatusParticipantBulk(array $ids, string $status): bool
    {
        $request = sprintf("UPDATE participants SET status = '%s' WHERE id in (%s)", $status, implode(',', $ids));
        DB::update($request);

        return true;
    }

    public function changeStatusParticipant(int $id, string $status): bool
    {
        $part = Participant::find($id);
        $part->status = $status;
        $part->save();

        return true;
    }

    public function inviteParticipantBulk(array $ids, ?string $message = null): bool
    {
        foreach ($ids as $id) {
            $part = Participant::find($id);
            if(!in_array($part->status, [ParticipantStatusEnum::DRAFT(), ParticipantStatusEnum::SERVICED()])) {
                self::inviteParticipant($part, $message);
            }
        }

        return true;
    }

    public function inviteParticipant(Participant $part, ?string $message = null): bool
    {
        try {
            $partUser = User::find($part->user_id);
            $partEvent = Event::find($part->event_id);

            $inviteResult = Mail::to($partUser->email)->send(new InvitePartMail($partEvent, $partUser, $message));
            if($inviteResult) {
                $part->status = ParticipantStatusEnum::INVITED();
                $part->save();
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
            return false;
        }

        return true;
    }
}
