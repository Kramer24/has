<?php

namespace App\Http\Requests;

use App\Enums\DocumentTypeEnum;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ContactInfoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'phone' => ['nullable', 'required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:9'],
            'viber' => ['nullable', 'required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:9'],
            'facebook' => ['nullable', 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'],
        ];

        return $rules;
    }
}
