<?php

namespace App\Http\Requests;

use App\Enums\DocumentTypeEnum;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name' => ['string', 'max:255'],
            'document_type' => ['required'],
            'document' => [
                'required',
                DocumentTypeEnum::getDocumentValidationMapping()[$this->request->get('document_type')],
                Rule::unique('users', 'document')
                    ->where(fn ($query) => $query
                        ->where('document',$this->request->get('document'))
                        ->where('document_type', $this->request->get('document_type'))
                    )
                    ->ignore(Auth::id()),
            ],
            'gender' => ['string', 'max:20'],
            'email' => ['email', 'max:255', Rule::unique(User::class)->ignore($this->user()->id)],
        ];

        return $rules;
    }
}
