<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Page;
use App\Models\Slider;
use TCG\Voyager\Traits\Translatable;

class PageController extends Controller
{
    public function index()
    {
        return view('index')->with([
            'sliders' => Slider::withTranslations()->where('is_active',true)->get()->sortByDesc('sort'),
            'events' => Event::orderBy('id','desc')->published()->take(3)->withTranslations()->get(),
            'aboutData' => Page::whereSlug('about-us')->first()
        ]);
    }

    public function about() {
        return view('pages.about')->with([
            'data' => Page::whereSlug('about-us')->first()
        ]);
    }

    public function release()
    {
        return view('pages.release');
    }
}
