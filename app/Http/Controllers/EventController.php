<?php

namespace App\Http\Controllers;

use App\Enums\ParticipantStatusEnum;
use App\Models\Event;
use App\Models\Participant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventController extends Controller
{
    public function index()
    {
        return view('events.index')->with([
            'events' => Event::withTranslations()->published()->simplePaginate(4),
            'recentEvents'=>Event::orderBy('id','desc')->published()->take(3)->withTranslations()->get()
        ]);
    }

    public function show(Event $event)
    {
        if(!in_array($event->status, ['PUBLISHED','FINISHED','REGISTRATION'])) {
            abort(403);
        }

        return view('events.show')->with([
            'event' => $event,
            'partCurrentEvent' => Participant::where(['user_id' => Auth::id(), 'event_id' => $event->id])->first(),
            'recentEvents'=>Event::orderBy('id','desc')->published()->take(3)->withTranslations()->get()
        ]);
    }

    public function onParticipate(Event $event, Request $request)
    {
        //TODO: Make custom request
        $data = $request->all();
        $user = User::find($data['user_id']);
//        if($event->isParticipant($user)) {
//            return redirect()->back()->with('warning','You are already participating in the event!');
//        }

        if($event->max_part && count($event->participations) >= $event->max_part) {
            $event->status = 'PUBLISHED';
            $event->save();

            return redirect()->back()->with('warning','Registration for the event is closed!');
        }

        $partDate =  [
            'status' => ParticipantStatusEnum::DRAFT(),
            'created_by' => $user->id,
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_by' => $user->id,
            'updated_at' => date('Y-m-d H:i:s', time()),
            'users' => []
        ];

        if(isset($data['selected-me'])) {
            $partDate['users']['user'] = $data['selected-me'];
        }
        if(isset($data['selected-children'])) {
            $partDate['users']['children'] = $data['selected-children'];
        }

        $event->participations()->detach($user->id);

        if(!empty($partDate['users'])) {
            $partDate['users'] = json_encode($partDate['users']);
            $event->participations()->attach($user->id, $partDate);
        }

        return redirect()->back()->with('success','Your request has been submitted. Admin check it out!');
    }
}
