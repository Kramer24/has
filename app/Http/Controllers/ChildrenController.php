<?php

namespace App\Http\Controllers;

use App\Models\Children;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ChildrenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.children');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $children = new Children();
        $children->fill($request->all());
        $children->save();

        $children->parents()->attach(Auth::id());

        return Redirect::route('children.index')->with('status', 'children-created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Children $children
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Children $children)
    {
        //TODO: Add validation for Parents and Admins
        $children->fill($request->all());
        $children->save();

        return Redirect::route('children.index')->with('status', 'children-updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Children $children
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Children $children)
    {
        //TODO: Add validation for Parents and Admins
        $children->delete();
        return back()->with('status', 'children-deleted');
    }
}
