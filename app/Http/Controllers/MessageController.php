<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessageRequest;
use App\Mail\ContactMessageMail;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class MessageController extends VoyagerBaseController
{
    public function addMessage(StoreMessageRequest $request)
    {
        $variant = 'success';
        $textResponse = 'Message is sent';
        $message = new Message();
        $message->fill([
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'message' => $request->message,
        ]);
        if ($message->save()) {
            Mail::to(setting('contacts.email_1'))->send(new ContactMessageMail($message));
        } else{
            $variant = 'error';
            $textResponse = 'Message is not sent';
        }
        return redirect()->route('index')->with($variant, $textResponse);
    }
}
