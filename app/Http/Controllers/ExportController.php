<?php

namespace App\Http\Controllers;

use App\Enums\ParticipantStatusEnum;
use App\Exports\ParticipantExport;
use App\Models\Event;
use App\Models\Participant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function exportParticipantsEvent(Event $event)
    {
        $acceptanceParticipants = DB::table('participants')
            ->select('users.name as userName','users.document as document', 'users')
            ->join('users', 'users.id', '=', 'participants.user_id')
            ->where('event_id', $event->id)
            ->whereIn('status', [ParticipantStatusEnum::ACCEPTED(), ParticipantStatusEnum::INVITED()])
            ->get()->toArray();

        return Excel::download(new ParticipantExport($acceptanceParticipants), 'participants-event-'.$event->id.'.xlsx');
    }
}
