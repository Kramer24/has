<?php

namespace App\Actions;

use App\Enums\ParticipantStatusEnum;
use App\Services\ParticipantService;
use TCG\Voyager\Actions\AbstractAction;

class ToServiceParticipants extends AbstractAction
{
    public function getTitle()
    {
        return '';
    }

    public function getIcon()
    {
        return 'voyager-person';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-dark pull-right ml-1',
        ];
    }

    public function getId()
    {
        return 'service-participants';
    }

    public function getDefaultRoute()
    {
        return route('voyager.participants.toService', $this->data->id);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'participants';
    }

    public function getTableName(): array
    {
        return ['dataTable-accepted','dataTable-invited'];
    }
}
