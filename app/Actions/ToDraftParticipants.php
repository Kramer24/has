<?php

namespace App\Actions;

use App\Enums\ParticipantStatusEnum;
use App\Services\ParticipantService;
use TCG\Voyager\Actions\AbstractAction;

class ToDraftParticipants extends AbstractAction
{
    public function getTitle()
    {
        return '';
    }

    public function getIcon()
    {
        return 'voyager-pause';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-warning pull-right ml-1',
        ];
    }

    public function getId()
    {
        return 'draft-participants';
    }

    public function getDefaultRoute()
    {
        return route('voyager.participants.toDraft', $this->data->id);
    }

    public function massAction($ids, $comingFrom)
    {
        try {
            if(empty(array_filter($ids))) {
                throw new \LogicException();
            }

            $partService = new ParticipantService();
            $result = $partService->changeStatusParticipantBulk($ids,ParticipantStatusEnum::DRAFT());
        } catch (\LogicException | \Exception $e) {
            $result = false;
        }

        $flashMessage = $result ? ['success' => 'Success!'] : ['error' => 'Error!'];

        return redirect()->back()->with($flashMessage);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'participants';
    }

    public function getTableName(): array
    {
        return ['dataTable-prepare','dataTable-accepted','dataTable-invited'];
    }
}
