<?php

namespace App\Actions;

use App\Enums\ParticipantStatusEnum;
use App\Services\ParticipantService;
use TCG\Voyager\Actions\AbstractAction;

class ToInviteParticipants extends AbstractAction
{
    public function getTitle()
    {
        return '';
    }

    public function getIcon()
    {
        return 'voyager-paper-plane';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-info pull-right ml-1',
        ];
    }

    public function getId()
    {
        return 'invite-participants';
    }

    public function getDefaultRoute()
    {
        return route('voyager.participants.toInvite', $this->data->id);
    }

    public function massAction($ids, $comingFrom)
    {
        try {
            if(empty(array_filter($ids))) {
                throw new \LogicException();
            }

            $partService = new ParticipantService();
            $result = $partService->inviteParticipantBulk($ids);
        } catch (\LogicException | \Exception $e) {
            $result = false;
        }

        $flashMessage = $result ? ['success' => 'Success!'] : ['error' => 'Error!'];

        return redirect()->back()->with($flashMessage);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'participants';
    }

    public function getTableName(): array
    {
        return ['dataTable-accepted','dataTable-invited'];
    }
}
