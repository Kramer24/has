<?php

namespace App\Actions;

use App\Enums\ParticipantStatusEnum;
use App\Services\ParticipantService;
use TCG\Voyager\Actions\AbstractAction;

class ToAcceptParticipants extends AbstractAction
{
    public function getTitle()
    {
        return '';
    }

    public function getIcon()
    {
        return 'voyager-check';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getId()
    {
        return 'accept-participants';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right ml-1',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.participants.toAccept', $this->data->id);
    }

    public function massAction($ids, $comingFrom)
    {
        try {
            if(empty(array_filter($ids))) {
                throw new \LogicException();
            }

            $partService = new ParticipantService();
            $result = $partService->changeStatusParticipantBulk($ids, ParticipantStatusEnum::ACCEPTED());
        } catch (\LogicException | \Exception $e) {
            $result = false;
        }

        $flashMessage = $result ? ['success' => 'Success!'] : ['error' => 'Error!'];

        return redirect()->back()->with($flashMessage);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'participants';
    }

    public function getTableName(): array
    {
        return ['dataTable-prepare','dataTable-invited'];
    }
}
