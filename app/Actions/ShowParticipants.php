<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ShowParticipants extends AbstractAction
{
    public function getTitle()
    {
        return 'Participants';
    }

    public function getIcon()
    {
        return 'voyager-people';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.participants.indexByEvent', $this->data->id);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'events';
    }
}
