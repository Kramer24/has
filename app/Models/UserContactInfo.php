<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContactInfo extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'user_contact_info';
    protected $fillable = ['user_id','viber','phone','facebook'];
    protected $primaryKey = 'user_id';

}
