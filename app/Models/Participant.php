<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Participant extends Model
{
    use HasFactory;

    protected $casts = [
        'users' => 'json',
    ];

    public function save(array $options = [])
    {
        if (!$this->created_by && Auth::user()) {
            $this->created_by = Auth::user()->id;
        }

        if (Auth::user()) {
            $this->updated_by = Auth::user()->id;
            $this->updated_at = date('Y-m-d H:i:s');
        }

        return parent::save();
    }
}
