<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingTranslation extends Model
{
    use HasFactory;

    protected $table = 'setting_translation';

    public $timestamps = false;

    public static function getTranslation($key, $locale = null)
    {
        if (!$locale) {
            $locale = app()->getLocale();
        }

        $translation = SettingTranslation::where('locale', $locale)
            ->where('key', $key)
            ->first();

        if ($translation) {
            return $translation->value;
        }

        return '';
    }
}
