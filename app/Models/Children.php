<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class Children extends Model
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $table = 'children';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'document_type',
        'document',
        'gender',
        'birthday',
    ];

    protected $dates = ['deleted_at'];

    public function save(array $options = [])
    {
        if (!$this->created_by && Auth::user()) {
            $this->created_by = Auth::user()->id;
        }

        if (Auth::user()) {
            $this->updated_by = Auth::user()->id;
        }

        return parent::save();
    }

    public function parents()
    {
        return $this->belongsToMany(User::class, 'user_children', 'children_id', 'user_id');
    }

    public function getFullNameAttribute() {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    public function getAgeAttribute() {
        $dob = new \DateTime($this->birthday);
        $now = new \DateTime();
        $diff = $now->diff($dob);
        return $diff->y;
    }
}
