<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Enums\ParticipantStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends \TCG\Voyager\Models\User
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'role_id',
        'document_type',
        'document',
        'gender',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvatarAttribute($value)
    {
        return $value ?? config('voyager.user.default_avatar', 'users/default.png');
    }

    public function contactInfo()
    {
        return $this->hasOne(UserContactInfo::class);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'participants', 'user_id',
            'event_id')->withPivot('status','created_by','updated_by')->withTimestamps();
    }

    public function lastServiceDate()
    {
         return $this->events()->select('participants.updated_at as lastServiceDate')
             ->where('participants.status',ParticipantStatusEnum::SERVICED())
             ->limit(1)
             ->orderByDesc('participants.updated_at')
             ->first()->lastServiceDate ?? null;
    }

    public function children()
    {
        return $this->belongsToMany(Children::class, 'user_children', 'user_id', 'children_id')
            ;
    }
}
