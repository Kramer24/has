<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Translation extends \TCG\Voyager\Models\Translation
{
    protected $table = 'translations';

    protected $fillable = ['table_name', 'column_name', 'foreign_key', 'locale', 'value'];
}
