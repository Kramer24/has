<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Traits\Translatable;

class Event extends Model
{
    use HasFactory;
    use Translatable;
    protected $translatable = [
        'title',
        'excerpt',
        'description',
    ];

    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->id;
        }

        if (!$this->created_by && Auth::user()) {
            $this->created_by = Auth::user()->id;
        }

        if (Auth::user()) {
            $this->updated_by = Auth::user()->id;
        }

        return parent::save();
    }

    public function delete(): bool
    {
        if(parent::delete()) {
            $this->participations()->detach();
        }

        return true;
    }

    public function scopePublished($query)
    {
        return $query->whereIn('status', ['published','finished','registration']);
    }

    public function participations()
    {
        return $this->belongsToMany(User::class,'participants', 'event_id', 'user_id');
    }

    public function isParticipant(User $user): bool
    {
        return $this->participations()->get()->contains($user);
    }
}
