<?php

namespace App\Enums;

use ArchTech\Enums\InvokableCases;
use ArchTech\Enums\Names;
use ArchTech\Enums\Values;

enum ParticipantStatusEnum
{
    use InvokableCases, Names, Values;

    case DRAFT;
    case REJECTED;
    case ACCEPTED;
    case INVITED;
    case SERVICED;

    public static function getStatusArray(): array
    {
        return [
            self::DRAFT() => 'draft',
            self::REJECTED() => 'rejected',
            self::ACCEPTED() => 'accepted',
            self::INVITED() => 'invited',
            self::SERVICED() => 'serviced',
        ];
    }
}
