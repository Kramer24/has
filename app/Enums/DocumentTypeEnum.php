<?php

namespace App\Enums;

use ArchTech\Enums\InvokableCases;
use ArchTech\Enums\Names;
use ArchTech\Enums\Values;

enum DocumentTypeEnum
{
    use InvokableCases, Names, Values;

    const TYPE_NLC = 'NLC';
    const TYPE_HUMANITARIAN_STATUS = 'HUMANITARIAN STATUS';

    public static function getDocumentTypes(): array
    {
        return [
            self::TYPE_NLC,
            self::TYPE_HUMANITARIAN_STATUS
        ];
    }

    public static function getDocumentValidationMapping(): array
    {
        return [
            self::TYPE_NLC => 'size:10',
            self::TYPE_HUMANITARIAN_STATUS => 'numeric',

        ];
    }
}
