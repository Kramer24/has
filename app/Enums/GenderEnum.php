<?php

namespace App\Enums;

use ArchTech\Enums\InvokableCases;
use ArchTech\Enums\Names;
use ArchTech\Enums\Values;

enum GenderEnum
{
    use InvokableCases, Names, Values;

    case MALE;
    case FEMALE;
    case ANOTHER;


    public static function getGendersArray(): array
    {
        return [
            self::MALE() => 'male',
            self::FEMALE() => 'female',
            self::ANOTHER() => 'another'
        ];
    }
}
