<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Models\Slider;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DemoRefreshCommand extends Command
{
    protected $signature = 'demo:refresh-database';

    protected $description = 'Clear all tables for demo';

    public function __construct()
    {
        parent::__construct();
    }
    public function handle(): int
    {
        $this->call('migrate:fresh');
        $this->call('db:seed');
        $this->call('view:clear');
        $this->call('cache:clear');
        (new Filesystem)->cleanDirectory(Storage::path(''));

        return self::SUCCESS;
    }
}
