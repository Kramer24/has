<?php

namespace App\Exports;

use App\Models\Children;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ParticipantExport implements FromArray, WithHeadings, ShouldAutoSize
{
    protected $array;

    public function __construct(array $array)
    {
        $result = [];
        foreach ($array as $key => $item) {
            if(isset(json_decode($item->users)->children)) {
                $children = Children::whereIn('id', json_decode($item->users)->children)->get();
                foreach ($children as $child) {
                    $result[] = [
                        'number' => ++$key,
                        'userName' => $item->userName,
                        'child' => $child->full_name,
                        'documentType' => $child->document_type,
                        'document' => $child->document
                    ];
                }
            }
            if(isset(json_decode($item->users)->user)) {
                $users = User::where('id', json_decode($item->users)->user)->get();
                if(isset($users)) {

                    foreach ($users as $user) {
                        $result[] = [
                            'number' => ++$key,
                            'userName' => $user->name,
                            'child' => '---',
                            'documentType' => $user->document_type,
                            'document' => $user->document
                        ];
                    }
                }

            }
        }
        $this->array = $result;
    }

    public function array(): array
    {
        return $this->array;
    }

    public function headings(): array
    {
        return [
            '#',
            'User Name',
            'Child',
            'Document Type',
            'Document',
            'Subscribe'
        ];
    }
}
