<?php

namespace App\Providers;

use App\Actions\ShowParticipants;
use App\Actions\ToAcceptParticipants;
use App\Actions\ToDraftParticipants;
use App\Actions\ToInviteParticipants;
use App\Actions\ToRejectParticipants;
use App\Actions\ToServiceParticipants;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Voyager::addAction(ShowParticipants::class);
        Voyager::addAction(ToAcceptParticipants::class);
        Voyager::addAction(ToRejectParticipants::class);
        Voyager::addAction(ToDraftParticipants::class);
        Voyager::addAction(ToInviteParticipants::class);
        Voyager::addAction(ToServiceParticipants::class);
    }
}
