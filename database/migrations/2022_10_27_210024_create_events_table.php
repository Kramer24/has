<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('excerpt');
            $table->text('description');
            $table->text('message');
            $table->integer('max_part')->unsigned()->nullable();
            $table->string('image')->nullable();
            $table->string('slug')->unique();
            $table->dateTime('event_start')->nullable();
            $table->dateTime('event_end')->nullable();
            $table->integer('age_from')->nullable();
            $table->integer('age_to')->nullable();
            $table->boolean('use_registration')->default(false);
            $table->integer('author_id');
            $table->enum('status', ['PUBLISHED', 'DRAFT', 'PENDING', 'FINISHED', 'REGISTRATION'])->default('DRAFT');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
};
