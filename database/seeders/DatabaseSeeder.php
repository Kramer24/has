<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Database\Seeders\Custom\ContactSettingsTableSeeder;
use Database\Seeders\Custom\EventBreadSeeder;
use Database\Seeders\Custom\MessageBreadSeeder;
use Database\Seeders\Custom\OurStoryBlockSettingsTableSeeder;
use Database\Seeders\Custom\ParticipantsBreadSeeder;
use Database\Seeders\Custom\PersonBlockSettingsTableSeeder;
use Database\Seeders\Custom\QuotesBlockSettingsTableSeeder;
use Database\Seeders\Custom\SettingTranslationBreadSeeder;
use Database\Seeders\Custom\SliderBreadSeeder;
use Database\Seeders\Voyager\CategoriesTableSeeder;
use Database\Seeders\Voyager\DataRowsTableSeeder;
use Database\Seeders\Voyager\DataTypesTableSeeder;
use Database\Seeders\Voyager\MenuItemsTableSeeder;
use Database\Seeders\Voyager\MenusTableSeeder;
use Database\Seeders\Voyager\PagesTableSeeder;
use Database\Seeders\Voyager\PermissionRoleTableSeeder;
use Database\Seeders\Voyager\PermissionsTableSeeder;
use Database\Seeders\Voyager\PostsTableSeeder;
use Database\Seeders\Voyager\RolesTableSeeder;
use Database\Seeders\Voyager\SettingsTableSeeder;
use Database\Seeders\Voyager\TranslationsTableSeeder;
use Database\Seeders\Voyager\UsersTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // Voyager seeders
            MenusTableSeeder::class,
            MenuItemsTableSeeder::class,
            CategoriesTableSeeder::class,
            DataTypesTableSeeder::class,
            DataRowsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionsTableSeeder::class,
            PermissionRoleTableSeeder::class,
            PagesTableSeeder::class,
            PostsTableSeeder::class,
            RolesTableSeeder::class,
            SettingsTableSeeder::class,
            TranslationsTableSeeder::class,
            UsersTableSeeder::class,
            SettingTranslationBreadSeeder::class,

            // Custom seeders
            ContactSettingsTableSeeder::class,
            EventBreadSeeder::class,
            MessageBreadSeeder::class,
            ParticipantsBreadSeeder::class,
            PersonBlockSettingsTableSeeder::class,
            SliderBreadSeeder::class,
            OurStoryBlockSettingsTableSeeder::class,
            QuotesBlockSettingsTableSeeder::class
        ]);
    }
}
