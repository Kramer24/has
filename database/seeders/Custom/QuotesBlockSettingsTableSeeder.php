<?php

namespace Database\Seeders\Custom;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class QuotesBlockSettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $setting = $this->findSetting('quotes.1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Quote 1',
                'value'        => 'Quote`s text',
                'details'      => '',
                'type'         => 'text',
                'order'        => 1,
                'group'        => 'Quotes',
            ])->save();
        }

        $setting = $this->findSetting('quotes.2');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Quote 2',
                'value'        => 'Quote`s text',
                'details'      => '',
                'type'         => 'text',
                'order'        => 1,
                'group'        => 'Quotes',
            ])->save();
        }

        $setting = $this->findSetting('quotes.3');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Quote 3',
                'value'        => 'Quote`s text',
                'details'      => '',
                'type'         => 'text',
                'order'        => 1,
                'group'        => 'Quotes',
            ])->save();
        }

        $setting = $this->findSetting('quotes.4');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Quote 4',
                'value'        => 'Quote`s text',
                'details'      => '',
                'type'         => 'text',
                'order'        => 1,
                'group'        => 'Quotes',
            ])->save();
        }

    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
