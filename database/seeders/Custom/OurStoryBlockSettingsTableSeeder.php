<?php

namespace Database\Seeders\Custom;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class OurStoryBlockSettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $setting = $this->findSetting('ourstory.image');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Image',
                'value'        => '',
                'details'      => '',
                'type'         => 'image',
                'order'        => 1,
                'group'        => 'OurStory',
            ])->save();
        }


        $setting = $this->findSetting('ourstory.founded_year');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Founded year',
                'value'        => '2000',
                'details'      => '',
                'type'         => 'text',
                'order'        => 2,
                'group'        => 'OurStory',
            ])->save();
        }

        $setting = $this->findSetting('ourstory.charities_rendered');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Charities rendered',
                'value'        => '50000',
                'details'      => '',
                'type'         => 'text',
                'order'        => 3,
                'group'        => 'OurStory',
            ])->save();
        }
    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
