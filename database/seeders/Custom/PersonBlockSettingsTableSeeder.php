<?php

namespace Database\Seeders\Custom;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class PersonBlockSettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $setting = $this->findSetting('personblock.title');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Block Title',
                'value'        => 'Block Title',
                'details'      => '',
                'type'         => 'text',
                'order'        => 1,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.sub_title');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Block Subtitle',
                'value'        => 'Block Subtitle',
                'details'      => '',
                'type'         => 'text',
                'order'        => 2,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.description');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Block Description',
                'value'        => 'Block Description',
                'details'      => '',
                'type'         => 'rich_text_box',
                'order'        => 3,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.link_twitter');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Twitter',
                'value'        => '#',
                'details'      => '',
                'type'         => 'text',
                'order'        => 4,
                'group'        => 'PersonBlock',
            ])->save();
        }
        $setting = $this->findSetting('personblock.link_facebook');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Facebook',
                'value'        => '#',
                'details'      => '',
                'type'         => 'text',
                'order'        => 5,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.link_instagram');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Instagram',
                'value'        => '#',
                'details'      => '',
                'type'         => 'text',
                'order'        => 6,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.image');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Image',
                'value'        => '',
                'details'      => '',
                'type'         => 'image',
                'order'        => 7,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.title1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Block Title 1',
                'value'        => 'Block Title 1',
                'details'      => '',
                'type'         => 'text',
                'order'        => 1,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.sub_title1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Block Subtitle 1',
                'value'        => 'Block Subtitle 1',
                'details'      => '',
                'type'         => 'text',
                'order'        => 2,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.description1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Block Description 1',
                'value'        => 'Block Description 1',
                'details'      => '',
                'type'         => 'rich_text_box',
                'order'        => 3,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.link_twitter1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Twitter 1',
                'value'        => '#',
                'details'      => '',
                'type'         => 'text',
                'order'        => 4,
                'group'        => 'PersonBlock',
            ])->save();
        }
        $setting = $this->findSetting('personblock.link_facebook1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Facebook 1',
                'value'        => '#',
                'details'      => '',
                'type'         => 'text',
                'order'        => 5,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.link_instagram1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Instagram 1',
                'value'        => '#',
                'details'      => '',
                'type'         => 'text',
                'order'        => 6,
                'group'        => 'PersonBlock',
            ])->save();
        }

        $setting = $this->findSetting('personblock.image1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Image 1',
                'value'        => '',
                'details'      => '',
                'type'         => 'image',
                'order'        => 7,
                'group'        => 'PersonBlock',
            ])->save();
        }
    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
