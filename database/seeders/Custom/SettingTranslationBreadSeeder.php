<?php

namespace Database\Seeders\Custom;

use App\Models\SettingTranslation;
use Illuminate\Database\Seeder;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class SettingTranslationBreadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'setting-translation');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'setting_translation',
                'slug'                  => 'setting-translation',
                'display_name_singular' => 'Setting Translation',
                'display_name_plural'   => 'Setting Translations',
                'icon'                  => '',
                'model_name'            => 'App\\Models\\SettingTranslation',
                'controller'            => 'App\\Http\\Controllers\\Voyager\\SettingTranslationController',
                'generate_permissions'  => 1,
                'description'           => '',
                'details'               => [
                    "order_column" => null, "order_display_column" => null,
                    "order_direction" => "asc", "default_search_key" => null
                ]
            ])->save();
        }

        $settingTranslationDataType = DataType::where('slug', 'setting-translation')->firstOrFail();
        $dataRow = $this->dataRow($settingTranslationDataType, 'id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'id',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($settingTranslationDataType, 'locale');


        $locales = LaravelLocalization::getSupportedLocales();

        $localeOptions = [];
        foreach ($locales as $localeCode => $properties) {
            $localeOptions[$localeCode] = $properties['name'];
        }
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'select_dropdown',
                'display_name' => 'Locale',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'default' => array_key_first($localeOptions),
                    'options' => $localeOptions,
                ],
                'order'        => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($settingTranslationDataType, 'key');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Key',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($settingTranslationDataType, 'value');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Value',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 4,
            ])->save();
        }

        $menu = Menu::where('name', 'admin')->firstOrFail();
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Setting Translation',
            'url'     => '',
            'route'   => 'voyager.setting-translation.index',
        ]);

        SettingTranslation::firstOrNew([
            'locale' => LaravelLocalization::getCurrentLocale(),
            'key' => 'personblock.title1',
            'value' => 'Sandy Chan',
        ]);
        SettingTranslation::insert([
            [
                'locale' => LaravelLocalization::getCurrentLocale(),
                'key' => 'personblock.title1',
                'value' => 'Sandy Chan',
            ],
            [
                'locale' => LaravelLocalization::getCurrentLocale(),
                'key' => 'personblock.sub_title1',
                'value' => 'Social worker',
            ],
            [
                'locale' => LaravelLocalization::getCurrentLocale(),
                'key' => 'personblock.title2',
                'value' => 'Sandy Chan',
            ],
            [
                'locale' => LaravelLocalization::getCurrentLocale(),
                'key' => 'personblock.sub_title2',
                'value' => 'Social worker',
            ]
        ]);

        Permission::generateFor('setting_translation');
        $role = Role::where('name', 'admin')->firstOrFail();
        $permissions = Permission::where(['table_name' => 'setting_translation'])->get();
        $role->permissions()->attach(
            $permissions->pluck('id')->all()
        );
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
