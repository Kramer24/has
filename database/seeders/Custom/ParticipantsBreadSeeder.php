<?php

namespace Database\Seeders\Custom;

use App\Enums\ParticipantStatusEnum;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class ParticipantsBreadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'participants');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'participants',
                'slug'                  => 'participants',
                'display_name_singular' => 'Participant',
                'display_name_plural'   => 'Participants',
                'icon'                  => 'voyager-people',
                'model_name'            => 'App\\Models\\Participant',
                'controller'            => 'App\\Http\\Controllers\\Voyager\\ParticipantController',
                'generate_permissions'  => 1,
                'description'           => '',
                'details'               => ['order_column' => 'id','order_display_column' => 'id','order_direction' => 'desc','default_search_key' => null,'scope' => null]
            ])->save();
        }

        $partDataType = DataType::where('slug', 'participants')->firstOrFail();
        $dataRow = $this->dataRow($partDataType, 'id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'id',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 1,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'event_id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'Event',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 2,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'participant_belongsto_event_relationship');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => 'Event',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    "model" => "App\\Models\\Event",
                    "table" => "events",
                    "type" => "belongsTo",
                    "column" => "event_id",
                    "key" => "id",
                    "label" => "title",
                    "pivot_table" => "",
                    "pivot" => "0",
                    "taggable" => "0"
                ],
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'participant_belongsto_event_relationship');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => 'Event',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    "model" => "App\\Models\\Event",
                    "table" => "events",
                    "type" => "belongsTo",
                    "column" => "event_id",
                    "key" => "id",
                    "label" => "title",
                    "pivot_table" => "",
                    "pivot" => "0",
                    "taggable" => "0"
                ],
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'user_id');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'User',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'order'        => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'users');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'text',
                'display_name' => 'Users',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'default' => ParticipantStatusEnum::DRAFT(),
                    'options' => ParticipantStatusEnum::getStatusArray(),
                ],
                'order' => 5,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'participant_belongsto_user_relationship');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => 'User',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    "model" => "App\\Models\\User",
                    "table" => "users",
                    "type" => "belongsTo",
                    "column" => "user_id",
                    "key" => "id",
                    "label" => "name",
                    "pivot_table" => "",
                    "pivot" => "0",
                    "taggable" => "0"
                ],
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'participant_belongsto_user_relationship');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'relationship',
                'display_name' => 'User',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    "model" => "App\\Models\\User",
                    "table" => "users",
                    "type" => "belongsTo",
                    "column" => "user_id",
                    "key" => "id",
                    "label" => "name",
                    "pivot_table" => "",
                    "pivot" => "0",
                    "taggable" => "0"
                ],
                'order'        => 3,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'status');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'select_dropdown',
                'display_name' => 'Status',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 1,
                'details'      => [
                    'default' => ParticipantStatusEnum::DRAFT(),
                    'options' => ParticipantStatusEnum::getStatusArray(),
                ],
                'order' => 4,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'created_at');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'timestamp',
                'display_name' => 'Created At',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 5,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'created_by');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'Created By',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 6,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'updated_at');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'timestamp',
                'display_name' => 'Updated At',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 7,
            ])->save();
        }

        $dataRow = $this->dataRow($partDataType, 'updated_by');
        if (!$dataRow->exists) {
            $dataRow->fill([
                'type'         => 'number',
                'display_name' => 'Updated By',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'order'        => 8,
            ])->save();
        }

        $menu = Menu::where('name', 'admin')->firstOrFail();
        $menuItem = MenuItem::firstOrNew([
            'menu_id' => $menu->id,
            'title'   => 'Participants',
            'url'     => '',
            'route'   => 'voyager.participants.index',
        ]);
        if (!$menuItem->exists) {
            $menuItem->fill([
                'target'     => '_self',
                'icon_class' => 'voyager-people',
                'color'      => null,
                'parent_id'  => null,
                'order'      => 17,
            ])->save();
        }

        Permission::generateFor('participants');
        $role = Role::where('name', 'admin')->firstOrFail();
        $permissions = Permission::where(['table_name' => 'participants'])->get();
        $role->permissions()->attach(
            $permissions->pluck('id')->all()
        );
    }


    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    /**
     * [dataRow description].
     *
     * @param [type] $type  [description]
     * @param [type] $field [description]
     *
     * @return [type] [description]
     */
    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
