<?php

namespace Database\Seeders\Custom;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Setting;

class ContactSettingsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $setting = $this->findSetting('contacts.email_1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Email 1',
                'value'        => 'test1@test.com',
                'details'      => '',
                'type'         => 'text',
                'order'        => 1,
                'group'        => 'Contacts',
            ])->save();
        }

        $setting = $this->findSetting('contacts.email_2');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Email 2',
                'value'        => 'test1@test.com',
                'details'      => '',
                'type'         => 'text',
                'order'        => 2,
                'group'        => 'Contacts',
            ])->save();
        }

        $setting = $this->findSetting('contacts.phone_1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Phone 1',
                'value'        => '77777777',
                'details'      => '',
                'type'         => 'text',
                'order'        => 3,
                'group'        => 'Contacts',
            ])->save();
        }

        $setting = $this->findSetting('contacts.phone_2');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Phone 2',
                'value'        => '88888888',
                'details'      => '',
                'type'         => 'text',
                'order'        => 4,
                'group'        => 'Contacts',
            ])->save();
        }

        $setting = $this->findSetting('contacts.address_1');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Address 1',
                'value'        => 'Address 1',
                'details'      => '',
                'type'         => 'text',
                'order'        => 5,
                'group'        => 'Contacts',
            ])->save();
        }

        $setting = $this->findSetting('contacts.address_2');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Address 2',
                'value'        => 'Address 2',
                'details'      => '',
                'type'         => 'text',
                'order'        => 6,
                'group'        => 'Contacts',
            ])->save();
        }

        $setting = $this->findSetting('contacts.map_code');
        if (!$setting->exists) {
            $setting->fill([
                'display_name' => 'Map code',
                'value'        => '',
                'details'      => '',
                'type'         => 'text_area',
                'order'        => 7,
                'group'        => 'Contacts',
            ])->save();
        }
    }

    /**
     * [setting description].
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    protected function findSetting($key)
    {
        return Setting::firstOrNew(['key' => $key]);
    }
}
