<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'test' => 'Тест.',
    'our_story' => 'Наша история',
    'our_mission' => 'Наша миссия',
    'salvation_army_in_bulgaria' => 'Армия Спасения в Болгарии',
    'founded' => 'Основано',
    'charities_rendered' => 'оказанных благотворительных услуг',
    'links' => 'Ссылки',
    'get_in_touch' => 'Свяжитесь с нами',
    'contact_information' => 'Контактная информация',
    'welcome_title' => 'Добро пожаловать в Армию Спасения - Болгария',
    'home' => 'Главная',
    'about' => 'О нас',
    'upcoming_events' => 'Ближайшие события',
    'events' => 'События',
    'contact' => 'Контакты',
    'name' => 'Имя',
    'first_name' => 'Имя',
    'surname' => 'Фамилия',
    'last_name' => 'Фамилия',
    'email' => 'Электронная почта',
    //
    'who_help' => 'Чем мы можем вам помочь?',
    'account' => 'Аккаунт',
    'send_message' => 'Отправить сообщение',
    'see_more' => 'Узнать больше',
    'start' => 'Начало',
    'end' => 'Конец',
    'become_volunteer' => 'Стать <strong>волонтером</strong>',
    'caring_love' => 'Заботиться с <strong>любовью</strong>',
    'make_donation' => 'Сделать <strong>пожертвование</strong>',
    'login' => 'Авторизация',
    'not_found_active_events' => 'Не найдено активных событий',
    'category' => 'Категория|Категории',
    'recent_events' => 'Недавние события',
    'event_details' => 'Подробности события',

    //
    'document_type' => 'Тип документа',
    'see_more_text' => 'Вы можете узнать о нас',
    'see_more_button' => 'подробнее',
    'age_restrictions' => 'Возрастные ограничения',

    //
    'you_already_registered' => 'Вы уже зарегистрированы!',
    'your_status' => 'Ваш статус',
    'change_choice' => 'Вы можете <b><a href="/" id="change-choice" class="text-gray-700">изменить</a></b> ваш выбор',
    'me' => 'Я',
    'event_finished' => 'Событие уже завершено',
    'select_for_register' => 'Выберите того, кого вы хотите записать на мероприятие',
    'register' => 'Зарегистрироваться',
    'age' => 'лет',
    'no_events' => 'Мероприятий на ближайшее время не запланированно',

    //
    'our_mission_text' => 'проповедовать Евангелие Иисуса Христа и восполнять основные человеческие нужды без дискриминации во имя Его.',
    'our_mission1' => 'Наше УЧЕНИЕ основано на Библии',
    'our_mission2' => 'Наше СЛУЖЕНИЕ движимо любовью Божьей',

    //
    'view_map' => 'Смотреть карту',
    'version' => 'Версия',

    'release_page_title' => 'Информация о релизах',

];
