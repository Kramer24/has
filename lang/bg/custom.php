<?php

return [



    'test' => 'Тест.',
    'our_story' => 'Нашата история',
    'our_mission' => 'Нашата мисия',
    'salvation_army_in_bulgaria' => '"Армията на Cпасението" в България',
    'founded' => 'Основан',
    'charities_rendered' => 'предоставени благотворителни услуги',
    'links' => 'Връзки',
    'get_in_touch' => 'Свържете се с нас',
    'contact_information' => 'Контакти',
    'welcome_title' => 'Добре дошли в "Армията на Cпасението" - България',
    'home' => 'Начало',
    'about' => 'За нас',
    'upcoming_events' => 'Предстоящи събития',
    'events' => 'Събития',
    'contact' => 'Контакти',
    'name' => 'Име',
    'first_name' => 'Име',
    'surname' => 'Презиме',
    'last_name' => 'Фамилия',
    'email' => 'Имейл',
    //
    'who_help' => 'Как можем да Ви помогнем?',
    'account' => 'Акаунт',
    'send_message' => 'Изпращане на съобщение',
    'see_more' => 'Научете повече',
    'start' => 'Начало',
    'end' => 'Край',
    'become_volunteer' => 'Станете <strong>доброволец</strong>',
    'caring_love' => 'Грижа с <strong>любов</strong>',
    'make_donation' => 'Направете <strong>дарение</strong>',
    'login' => 'Влезте',
    'not_found_active_events' => 'Няма намерени настоящи събития',
    'category' => 'Категории',
    'recent_events' => 'Последни събития',
    'event_details' => 'Подробности за събитието',

    //
    'document_type' => 'Документ',
    'see_more_text' => 'Повече за нас',
    'see_more_button' => 'още',
    'age_restrictions' => 'Възрастови ограничения',

    //
    'you_already_registered' => 'Вече сте регистрирани!',
    'your_status' => 'Вашето състояние',
    'change_choice' => 'Можете да <b><a href="/" id="change-choice" class="text-gray-700">промените</a></b> своя избор',
    'me' => 'аз',
    'event_finished' => 'Събитието вече е минало',
    'select_for_register' => 'Изберете кого искате да регистрирате за събитието',
    'register' => 'Регистриране',
    'age'=> 'години',
    'no_events' => 'Няма събития в близко бъдеще',

    //
    'our_mission_text' => 'Да проповядваме Евангелието на Исус Христос и да посрещаме основни човешки нужди без дискриминация в Неговото име.',
    'our_mission1' => 'Нашите УЧЕНИЯ се основават на Библията',
    'our_mission2' => 'Нашето СЛУЖЕНИЕ се ръководи от Божията любов',

    //
    'view_map' => 'Преглед на картата',
    'version' => 'Версия',

    'release_page_title' => 'Заглавие',
];
