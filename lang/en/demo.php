<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'main_title' => 'HADAS',
    'welcome_title' => 'Welcome to service - HADAS',
    'our_mission_text' => 'Sed leo nisl, posuere at molestie ac, suscipit auctor quis metus',
    'our_mission1' => 'Charity Theme',
    'our_mission2' => 'Fast Support',



    'test' => 'Test.',
    'our_story' => 'Our story',
    'our_mission' => 'Our mission',
    'salvation_army_in_bulgaria' => 'The Salvation Army in Bulgary',
    'founded' => 'Founded',
    'charities_rendered' => 'charities rendered',
    'links' => 'Links',
    'get_in_touch' => 'Get in touch',
    'contact_information' => 'Contact Information',
    'home' => 'Home',
    'about' => 'About',
    'upcoming_events' => 'Upcoming events',
    'events' => 'Events',
    'contact' => 'Contact',
    'name' => 'Name',
    'first_name' => 'Name',
    'surname' => 'Surname',
    'last_name' => 'Surname',
    'email' => 'Email',
    //
    'who_help' => 'What can we help you?',
    'account' => 'Account',
    'send_message' => 'Send message',
    'see_more' => 'See more',
    'start' => 'Start',
    'end' => 'End',
    'become_volunteer' => 'Become a <strong>volunteer</strong>',
    'caring_love' => 'Caring with <strong>love</strong>',
    'make_donation' => 'Make a <strong>Donation</strong>',
    'login' => 'Login',
    'not_found_active_events' => 'Not fount active events',
    'category' => 'Category|Categories',
    'recent_events' => 'Recent events',
    'event_details' => 'Event details',

    //
    'document_type' => 'Document type',
    'see_more_text' => 'You can find out more about us',
    'see_more_button' => 'more',
    'age_restrictions' => 'Age restrictions',

    //
    'you_already_registered' => 'You are already registered!',
    'your_status' => 'Your status',
    'change_choice' => 'You can <b><a href="/" id="change-choice" class="text-gray-700">change</a></b> your choice',
    'me' => 'I',
    'event_finished' => 'Event has already ended',
    'select_for_register' => 'Select who you want to register for the event',
    'register' => 'Register',
    'age' => 'years',

    //
    'view_map' => 'View map',
    'version' => 'Version',

];
