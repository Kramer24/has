<?php
 return [
     'personblock' => [
         'description' => 'Personal block Description',
         'title' => 'Sandy Chan',
         'worktime' => 'Monday - Friday - 10:00-15:00',
     ],
     'main_title' => 'Armata Salvarii Bulgaria',
     'main_sub_title' => 'NON-PROFIT ORGANIZATION',
 ];
