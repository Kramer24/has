<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'test' => 'Test.',
    'our_story' => 'Povestea noastră',
    'our_mission' => 'Misiunea noastră',
    'salvation_army_in_bulgaria' => 'Armata Salvării în Bulgaria',
    'founded' => 'Fondat',
    'charities_rendered' => 'de servicii caritabile prestate',
    'links' => 'Lincuri',
    'get_in_touch' => 'Contactați-ne',
    'contact_information' => 'Informații de contact',
    'welcome_title' => 'Bine ați venit la Armata Salvării - Bulgaria',
    'home' => 'Acasă',
    'about' => 'Despre noi',
    'upcoming_events' => 'Evenimente viitoare',
    'events' => 'Evenimente',
    'contact' => 'Contacte',
    'name' => 'Nume',
    'first_name' => 'Nume',
    'surname' => 'Nume de familie',
    'last_name' => 'Nume de familie',
    'email' => 'E-mail',
    //

    'document_type' => 'Tipul documentului',
    'see_more_text' => 'Poți afla mai multe despre noi',
    'see_more_button' => 'mai mult',

    //

];
